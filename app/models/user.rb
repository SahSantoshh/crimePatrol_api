class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  # :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  # image
  has_attached_file :avatar,
                    url: '/profile/:id/:filename'

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  def send_reset_password_instructions(opts = nil)
    token = genrate_custom_token
    opts ||= {}
    # fall back to "default" config name
    opts[:client_config] ||= 'default'

    send_devise_notification(:reset_password_instructions, token, opts)
  end

  def genrate_custom_token
    token = SecureRandom.base58(8)
    token_enc = Devise.token_generator.digest(self, :reset_password_token, token)
    self.reset_password_token   = token_enc
    self.reset_password_sent_at = Time.now.utc
    save(validate: false)
    token
  end

  def update_password(password)
    self.password = password
    save
  end
end
