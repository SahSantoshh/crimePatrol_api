class Crime < ApplicationRecord
  # image
  has_attached_file :avatar,
                    url: '/crime/:id/:filename'

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
end
