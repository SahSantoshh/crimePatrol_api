module Api
  module V1
    class CrimesController < ApplicationController
      before_action :set_crime, only: %i[show update destroy]
      before_action :authenticate_api_v1_user!, only: %i[update destroy]

      # GET /crimes
      def index
        @crimes = Crime.all

        render json: @crimes
      end

      # GET /crimes/1
      def show
        render json: @crime
      end

      # POST /crimes
      def create
        @crime = Crime.new(crime_params)

        if @crime.save
          render json: @crime
        else
          render json: @crime.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /crimes/1
      def update
        if @crime.update(crime_params)
          render json: @crime
        else
          render json: @crime.errors, status: :unprocessable_entity
        end
      end

      # DELETE /crimes/1
      def destroy
        @crime.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_crime
        @crime = Crime.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def crime_params
        params.permit(:lat, :lng, :solved, :avatar, :title, :description)
      end
    end
  end
end
