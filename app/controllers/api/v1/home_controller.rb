module Api
  module V1
    class HomeController < ApplicationController
      def index
        crime_types = CrimeType.all
        render json: crime_types
      end
    end
  end
end
