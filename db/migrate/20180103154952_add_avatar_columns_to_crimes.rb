class AddAvatarColumnsToCrimes < ActiveRecord::Migration[5.1]
  def up
    add_attachment :crimes, :avatar
  end

  def down
    remove_attachment :crimes, :avatar
  end
end
