class AddTitleAndDescToCrimes < ActiveRecord::Migration[5.1]
  def up
    add_column :crimes, :title, :string
    add_column :crimes, :description, :text
    change_column_null :crimes, :lat, false
    change_column_null :crimes, :lng, false
  end

  def down
    remove_column :crimes, :title
    remove_column :crimes, :description
    change_column_null :crimes, :lat, true
    change_column_null :crimes, :lng, true
  end
end
