class CreateCrimes < ActiveRecord::Migration[5.1]
  def change
    create_table :crimes, id: :uuid do |t|
      t.decimal :lat, precision: 10, scale: 6
      t.decimal :lng, precision: 10, scale: 6
      t.integer :solved, default: 0
      t.timestamps
    end
  end
end
