class CreateCrimeTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :crime_types, id: :uuid do |t|
      t.string :name

      t.timestamps
    end
  end
end
