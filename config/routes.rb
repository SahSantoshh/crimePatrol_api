Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'reset_password' => 'reset_password#password_reset'
      mount_devise_token_auth_for 'User', at: 'auth'

      resources :crimes
      # Home
      resources :home
      # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    end
  end
end
