require 'test_helper'

class CrimesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @crime = crimes(:one)
  end

  test "should get index" do
    get crimes_url, as: :json
    assert_response :success
  end

  test "should create crime" do
    assert_difference('Crime.count') do
      post crimes_url, params: { crime: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show crime" do
    get crime_url(@crime), as: :json
    assert_response :success
  end

  test "should update crime" do
    patch crime_url(@crime), params: { crime: {  } }, as: :json
    assert_response 200
  end

  test "should destroy crime" do
    assert_difference('Crime.count', -1) do
      delete crime_url(@crime), as: :json
    end

    assert_response 204
  end
end
